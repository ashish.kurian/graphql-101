const express = require("express");
const morgan = require("morgan");
const bp = require("body-parser");
const app = express();

app.use(morgan("combined"));
app.use(bp.urlencoded({ extended: false }));
app.use(bp.json());

const { port } = require("./config");
let { cars, companies } = require("./data");

// GET endpoints ---------------------------------------------------------------
app.get("/cars", (req, res) => {
  res.json({ data: cars.map(c => ({ id: c.id, name: c.name })) });
});

app.get("/companies", (req, res) => {
  res.json({ data: companies.map(c => ({ id: c.id, name: c.name })) });
});

app.get("/cars/:id", (req, res) => {
  const car = cars.find(c => c.id === parseInt(req.params.id)) || {};
  res.json({ data: car });
});

app.get("/companies/:id", (req, res) => {
  const company = companies.find(c => c.id === parseInt(req.params.id)) || {};
  res.json({ data: company });
});
// -----------------------------------------------------------------------------

// POST endpoints --------------------------------------------------------------
// add one car
app.post("/cars", (req, res) => {
  const car = { ...req.body };
  car.id = cars.length + 1;
  cars.push(car);
  res.json({ data: car.id });
});
// add one company
app.post("/companies", (req, res) => {
  const company = { ...req.body };
  const last_id = companies[companies.length - 1]["id"];
  company.id = last_id + 1;
  companies.push(company);
  res.json({ data: company.id });
});
// -----------------------------------------------------------------------------

// DELETE endpoints-------------------------------------------------------------
app.delete("/cars/:id", (req, res) => {
  cars = cars.filter(c => c.id !== parseInt(req.params.id));
  res.json({});
});

app.delete("/companies/:id", (req, res) => {
  companies = companies.filter(c => c.id !== parseInt(req.params.id));
  res.json({});
});
// -----------------------------------------------------------------------------

// PUT endpoints ---------------------------------------------------------------
app.put("/cars/:id", (req, res) => {
  const index = cars.map(c => c.id).indexOf(parseInt(req.params.id));
  cars[index] = {
    id: parseInt(req.params.id),
    name: req.body.name,
    year: req.body.year,
    company_id: req.body.company_id
  };
  res.json({});
});

app.put("/companies/:id", (req, res) => {
  const index = companies.map(c => c.id).indexOf(parseInt(req.params.id));
  companies[index] = {
    id: parseInt(req.params.id),
    name: req.body.name,
    founded_year: req.body.founded_year
  };
  res.json({});
});
// -----------------------------------------------------------------------------

// get a list of cars from a company
app.get("/companies/:id/cars", (req, res) => {
  const data = cars.filter(c => c.company_id === parseInt(req.params.id));
  res.json({ data });
});

app.listen(port, err => {
  if (err) {
    console.log(err);
  }
  console.log(`app running on ${port}`);
});
