const cars = [
  {
    id: 1,
    name: "Ambassador",
    year: 1958,
    company_id: 10
  },
  {
    id: 2,
    name: "Contessa",
    year: 1984,
    company_id: 10
  },
  {
    id: 3,
    name: "Zen",
    year: 1993,
    company_id: 11
  },
  {
    id: 4,
    name: "800",
    year: 1983,
    company_id: 11
  }
];

const companies = [
  {
    id: 10,
    name: "Hindustan Motors",
    founded_year: 1942
  },
  {
    id: 11,
    name: "Maruti Suzuki",
    founded_year: 1981
  }
];

module.exports = {
  companies,
  cars
};
