# Introduction to GraphQL

- GraphQL is a spec like SQL - no not like MySQL or PostgreSQL, but plain SQL, the language to 
  query databases.
- Its an alternative to REST. But its power shows when querying related data.
- We'll be building the graphql clone for [this](./rest/README.md) codebase.

## Links

- https://learn.hasura.io/
- https://graphql.org/learn/
- https://www.apollographql.com/docs/
- https://www.youtube.com/watch?v=783ccP__No8

## Slides

- [Google Slides](https://docs.google.com/presentation/d/1iJCdGQg_jBPInw4nxyxvAxXIp_BT-7X2-4TEqHnF0Ms/edit?usp=sharing)
