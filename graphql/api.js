const express = require("express");
const expressGraphQL = require("express-graphql");
const morgan = require("morgan");
const bp = require("body-parser");
const app = express();

const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLNonNull,
  GraphQLInt
} = require("graphql");

app.use(morgan("combined"));
app.use(bp.urlencoded({ extended: false }));
app.use(bp.json());

const { port } = require("./config");
let { cars, companies } = require("./data");

const CompanyType = new GraphQLObjectType({
  name: "Company",
  description: "This represents a company.",
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLInt)
    },
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    founded_year: {
      type: GraphQLNonNull(GraphQLInt)
    },
    cars: {
      type: new GraphQLList(CarType),
      resolve: company => {
        return cars.filter(c => c.company_id === company.id);
      }
    }
  })
});

const CarType = new GraphQLObjectType({
  name: "Car",
  description: "This represents a car.",
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLInt)
    },
    name: {
      type: GraphQLNonNull(GraphQLString)
    },
    year: {
      type: GraphQLNonNull(GraphQLInt)
    },
    company_id: {
      type: GraphQLNonNull(GraphQLInt)
    },
    company: {
      type: CompanyType,
      resolve: car => {
        return companies.find(c => c.id === car.company_id);
      }
    }
  })
});

const RootQueryType = new GraphQLObjectType({
  name: "Query",
  description: "Root Query",
  fields: () => ({
    car: {
      type: CarType,
      description: "A single car",
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve: (parent, args) => cars.find(c => c.id === args.id)
    },
    cars: {
      type: new GraphQLList(CarType),
      description: "Lists all cars",
      resolve: () => cars
    },
    company: {
      type: CompanyType,
      description: "A single company",
      args: {
        id: { type: GraphQLInt }
      },
      resolve: (parent, args) => companies.find(c => c.id === args.id)
    },
    companies: {
      type: new GraphQLList(CompanyType),
      description: "List of companies",
      resolve: () => companies
    }
  })
});

const RootMutationType = new GraphQLObjectType({
  name: "Mutation",
  description: "Root mutation",
  fields: () => ({
    addCar: {
      type: CarType,
      description: "Adds a car",
      args: {
        name: { type: GraphQLNonNull(GraphQLString) },
        company_id: { type: GraphQLNonNull(GraphQLInt) },
        year: { type: GraphQLNonNull(GraphQLInt) }
      },
      resolve: (parent, args) => {
        const car = {
          id: cars.length + 1,
          name: args.name,
          company_id: args.company_id,
          year: args.year
        };
        cars.push(car);
        return car;
      }
    },
    addCompany: {
      type: CompanyType,
      description: "Adds a company",
      args: {
        name: { type: GraphQLNonNull(GraphQLString) },
        founded_year: { type: GraphQLNonNull(GraphQLInt) }
      },
      resolve: (parent, args) => {
        const company = {
          id: companies.length + 1,
          name: args.name,
          founded_year: args.year
        };
        companies.push(company);
        return company;
      }
    },
    deleteCar: {
      type: CarType,
      description: "Deletes a car",
      args: {
        id: { type: GraphQLNonNull(GraphQLInt) }
      },
      resolve: (parent, args) => {
        const car = cars.find(c => c.id === args.id);
        cars = cars.filter(c => c.id !== args.id);
        return car;
      }
    },
    deleteCompany: {
      type: CompanyType,
      description: "Deletes a company",
      args: {
        id: { type: GraphQLNonNull(GraphQLInt) }
      },
      resolve: (parent, args) => {
        const company = companies.find(c => c.id === args.id);
        companies = companies.filter(c => c.id !== args.id);
        return company;
      }
    },
    updateCompany: {
      type: CompanyType,
      description: "Updates a company",
      args: {
        id: { type: GraphQLNonNull(GraphQLInt) },
        name: { type: GraphQLNonNull(GraphQLString) },
        founded_year: { type: GraphQLNonNull(GraphQLInt) }
      },
      resolve: (parent, args) => {
        const companyIndex = companies.map(c => c.id).indexOf(args.id);
        const company = {
          id: args.id,
          name: args.name,
          founded_year: args.founded_year
        };
        companies[companyIndex] = company;
        return company;
      }
    },
    updateCar: {
      type: CarType,
      description: "Updates a car",
      args: {
        id: { type: GraphQLNonNull(GraphQLInt) },
        name: { type: GraphQLNonNull(GraphQLString) },
        year: { type: GraphQLNonNull(GraphQLInt) },
        company_id: { type: GraphQLNonNull(GraphQLInt) }
      },
      resolve: (parent, args) => {
        const carIndex = cars.map(c => c.id).indexOf(args.id);
        const car = {
          id: args.id,
          name: args.name,
          year: args.year,
          company_id: args.company_id
        };
        companies[carIndex] = car;
        return car;
      }
    }
  })
});

const schema = new GraphQLSchema({
  query: RootQueryType,
  mutation: RootMutationType
});

app.use(
  "/graphql",
  expressGraphQL({
    graphiql: true,
    schema
  })
);

app.listen(port, err => {
  if (err) {
    console.log(err);
  }
  console.log(`app running on ${port}`);
});
